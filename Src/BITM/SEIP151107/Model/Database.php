<?php

namespace App\Model;

use PDO;
use PDOException;

class Database
{
    public $DBH;
    public $host = "localhost";
    public $dbname = "atomic_project_b36";
    public $username = "root";
    public $password = "";

    public function __construct()
        {
            try
        {
            # MySQL with PDO_MYSQ
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname",
                $this->username, $this->password);
            echo "Database successfully connected !!!";
        }
            catch(PDOException $e)
            {
                echo $e->getMessage();
            }
        }
}